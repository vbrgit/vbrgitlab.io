+++
date = "2016-04-20T00:00:00"
draft = false
title = "about"
section_id = 0
weight = 0
+++

# About me

I received a Ph.D. in Applied Mathematics at Universidad Carlos III de Madrid (UC3M) in 2013. Then I was a Postdoctoral fellow (2013-2015) in the Advanced Study Program (ASP) at the [National Center for Atmospheric Research](https://ncar.ucar.edu/), Boulder, CO (USA), and held research positions (2015-2017) in the Numerical Aspects Section at [ECMWF](https://www.ecmwf.int/), Reading (UK), and the [Department of Mathematical Sciences](https://www.bath.ac.uk/departments/department-of-mathematical-sciences/) at University of Bath (UK).

I am currently a visiting professor in the Department of Mathematics at UC3M. My research focuses on the development and application of numerical methods for the solution of differential equations, where I am particularly interested in meshfree methods based on radial basis functions (RBFs). 
